#ifndef _PT_MEM_H
#define _PT_MEM_H
#include "pt52io.h"
#include <string.h>

#if 0
void *pt_realloc(void *block, size_t size);
void *pt_calloc(size_t num, size_t nsize);
void *pt_malloc(size_t size);
void pt_free(void *block);
void pt_reset_memory();
#else
#include "umm_malloc/umm_malloc.h"
void UART_RESET();
#if PICO_RP2350
#define HEAP_SIZE (1024 * 400)
#else
#define HEAP_SIZE (1024 * 175)
#endif
extern  char HEAP[HEAP_SIZE];
#define pt_realloc  umm_realloc
#define pt_calloc   umm_calloc
#define pt_malloc   umm_malloc
#define pt_free     umm_free
#define pt_reset_memory(...) (UART_RESET(),umm_init_heap(HEAP, HEAP_SIZE))
#endif
uint32_t pt_get_used_memory();
uint32_t pt_get_free_memory();

#endif