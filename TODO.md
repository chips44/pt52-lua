# TODO

## Replace text editor

- [X] Write new text editor

## Sprite drawing

- [X] Multiple colour depths  
- [X] Y scaling  
- [X] X scaling  
- [ ] Drawing when off screen  

## Text mode 2  

- [X] Configuration registers _*_  
- [X] Switch for 80/40 col mode  
- [X] Draw a cursor
- [X] Redirect functions for uses with this mode  
- [X] screen reset
- [X] scroll up
- [?] scroll down
- [X] put character
- [X] clear screen
- [X] clear line

### Text mode 2 registers  

- [X] 80/40 Columns mode  
- [X] Use separate background pallet  

## GFX Library

- [X] GFX Library  

## Sprite Library

- [X] Fix API
- [X] Add sprite datatype

## VMEM Library  

- [X] Resource Loading  
- [X] Font binaries  
- [X] Images  
- [X] Sprites  
- [X] CLUT  
- [X] Use Registers
- [X] Basic Read/Write of all VRAM
- [X] Selectable Ranges for VRAM Operations
- [X] Allow a table of data to be written in one operation
- [X] Bulk Read data into a table
- [ ] Read/Write 16/32/64 bit numbers (store/fetch)
- [ ] ~~Register Functions~~  
- [X] Memory operations (save/load) Blocks
- [X] Font ROM operations  

## Sound Library

- [ ] Mute command for bit mask channel(s)  
- [ ] Volume control pre channel
- [ ] Non blocking music sequencer  

## LOW Priority

- [ ] Sound?  
- [ ] Graphics modes 4 ?  
- [ ] ~~Tile based~~  
