#ifndef _OVERLAY_H
#define _OVERLAY_H

#include <stdio.h>
#include "pico.h"
#include "pico/stdlib.h"
#include "attributes.h"
#include <stdarg.h>
#include "font.h"
#include "config.h"

enum Overlay_CLUT_Source_t {
    OVERLAY_INTERNAL = 0,
    OVERLAY_CLUT_WINDOWED,
    OVERLAY_SPRITE,
};

enum Overlay_Pallets_t {
    OVERLAY_PAL_SYS_NO_TRANS = 0,
    OVERLAY_PAL_SYS_TRANS,
};

void Overlay_init();
void Overlay_load_clut(enum Overlay_Pallets_t Pallet);
void Overlay_clear(uint8_t colour_index);
/**
 * put a character at a location.
 * 
 * \param x 
 * \param y 
 * \param colour
 * \param character what to place
 * \return none
 */
void Overlay_putchar(uint8_t x, uint8_t y, uint8_t colour, uint8_t Font, const char character);
/**
 * put a string at a location.
 * 
 * \param x 
 * \param y 
 * \param colour
 * \param format
 * \return none
 */
void Overlay_print(uint8_t x, uint8_t y, uint8_t colour, const char *format, ...);

void Overlay_pixel_plotter(uint16_t x, uint16_t y, uint8_t colour);
#define Overlay_draw_line(x0, y0, x1, y1, c) draw_line(Overlay_pixel_plotter,x0, y0, x1, y1, c)
#define Overlay_draw_rect(x,y,w,h,c) draw_rect(Overlay_pixel_plotter,x,y,w,h,c)
#define Overlay_fill_rect(x,y,w,h,c) fill_rect(Overlay_pixel_plotter,x,y,w,h,c)
#define Overlay_draw_circle(x,y,r,c) draw_circle(Overlay_pixel_plotter,x,y,r,c)
#define Overlay_fill_circle(x,y,r,c) fill_circle(Overlay_pixel_plotter,x,y,r,c)
#define Overlay_draw_ellipse(x,y,rx,ry,c) draw_ellipse(Overlay_pixel_plotter,x,y,rx,ry,c)
#define Overlay_draw_polygon(n,v,c) draw_polygon(Overlay_pixel_plotter,n.v,c)
#define Overlay_drawCircleHelper(x,y,r,n,c) drawCircleHelper(Overlay_pixel_plotter,x,y,r,n,c)
#define Overlay_drawRoundRect(x,y,w,h,r,c) drawRoundRect(Overlay_pixel_plotter,x,y,w,h,r,c)
#endif