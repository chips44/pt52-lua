#ifndef PT52IO_H
#define PT52IO_H

#include "pico.h"
#if 0
/* Lua Print functions */

#define lua_writestring(s,l)   printf(("%s",s))

/* print a newline and flush the output */
#define lua_writeline()        (puts(""))

#define lua_writestringerror(s,l) printf((s,l))
#define LUAI_ASSERT
#else
#include "charbuffer_improved.h"
#include "attributes.h"
#include "overlay.h"
#include "draw.h"
//#include "uart.h"
#include "simple_system.h"
#include "video_driver.h"
#include "font.h"
#include "fatfs/ff.h"
#include "sdcard/ff_stdio.h"
#include "programs/ptsh/ptsh.h"
#include "pt_mem.h"

void Set_HEADER(const char *format, ...);//const char *Text);
void Set_FOOTER(const char *format, ...);//const char *Text);
void CLR_HEADER();
void CLR_FOOTER();

#include <stdlib.h>

/* Lua Print functions */

#define lua_writestring(s,l)   Printf(("%s",s))

/* print a newline and flush the output */
#define lua_writeline()        (Puts(""))

#define lua_writestringerror(s,l) (Printf((s),(l)),Puts(""))

#ifdef ENABLE_VT100
#define ESC_INDEX_LIMIT 8

#ifndef FEMTO_PICO_LIB
char* readline(const char* p);
#define readline_free(p) free(p)
#else
#include "femto/src/LineEditor.h"
#define readline(p) Line_Editor_ReadLine(p)
#define readline_free(p) (void)p
#endif

// ESC Status
enum eESC_mode
{
    ESC_OFF     = 0,
    ESC_ONLY    = 1,    // ESC
    ESC_CSI     = 2,    // ESC [
    ESC_CSI_Q   = 3,    // ESC [?
    // ESC_CONF    = 4,    // ESC [ ... {h|l} 
    ESC_G0      = 5,    // ESC (
    ESC_G1      = 6,    // ESC )
    ESC_AMB     = 7,    // ESC #
    ESC_DCS     = 8,    // ESC P
};
#endif

#endif
#endif