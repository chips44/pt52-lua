/**
 * BSD 3-Clause License
 * 
 * Copyright (c) 2021, DarkElvenAngel
 * All rights reserved.
 * 
 * see LICENCE for full Licence
 */
#include <stdio.h>
#include "pico.h"
#include "pico/stdlib.h"
#include "charbuffer_improved.h"

/**
 * Set background colour
 * 
 * \param colour value 0 - 15
 * \return none
 */
void Set_BColour(uint16_t colour)
{
#ifdef ENABLE_VT100
    if (colour < 8 && Cursor.intensity) colour + 8;
#endif
    if (Cursor.reverse)
        Cursor.foreground_colour = colour % 256;
    else
        Cursor.background_colour = colour % 256;
}

/**
 * Set foreground colour
 * 
 * \param colour value 0 - 15
 * \return none
 */
void Set_FColour(uint16_t colour)
{
#ifdef ENABLE_VT100
    if (colour < 8 && Cursor.intensity) colour + 8;
#endif
    if (Cursor.reverse)
        Cursor.background_colour = colour % 256;
    else
        Cursor.foreground_colour = colour % 256;
}
/**
 * Set both colours
 * 
 * \param colours
 * \return none
 */
void Set_Colours(uint32_t colours)
{
    if (Cursor.reverse)
        Cursor.colours = (colours & 0xFF) << 8 | (colours & 0xFF00 ) >> 4;
    else
        Cursor.colours = colours % 0xFFFF;
}
/**
 * Set pallet
 * 
 * \param index value 0 - 4
 * \return none
 */
void Set_Pallet(uint8_t index)
{
    Cursor.Pallet = index % 5;
}

void Set_Attrib_Underline(bool value)
{
    Cursor.underline = value ;
}
/**
 * Set Attribute Reverse
 * 
 * \param value boolean
 * \return none
 */
void Set_Attrib_Reverse(bool value)
{
    if (Cursor.reverse != value)
    {
        uint16_t holder = Cursor.background_colour;
        Cursor.background_colour = Cursor.foreground_colour;
        Cursor.foreground_colour = holder;
    }
    Cursor.reverse = value ;
};
/**
 * Set Attribute Bold
 * 
 * \param value boolean
 * \return none
 */
void Set_Attrib_Bold(bool value)
{
    Cursor.Use_Font = value ? 1 : 0;
}
/**
 * Set Attribute Extended
 * 
 * \param value boolean
 * \return none
 */
void Set_Attrib_Extended(bool value)
{
    Cursor.extended = value ;
}
/**
 * Set Attribute Code Page 437 font
 * 
 * \param value boolean
 * \return none
 */
void Set_Attrib_codepage437(bool value)
{
    Cursor.Use_Font = value ? 2 : 0;
}
/**
 * Set Attribute Uses Font Buffer
 * 
 * \param value boolean
 * \return none
 */
void Set_Attrib_fontBuffer(bool value)
{
    Cursor.Use_Font = value ? 3 : 0;
}
/**
 * Set Font
 * 
 * \param value boolean
 * \return none
 */
void Set_Attrib_Font(uint8_t value)
{
    Cursor.Use_Font = value;
}
#ifdef ENABLE_VT100
/**
 * Set Attribute Intensity
 * 
 * \param value boolean
 * \return none
 */
void Set_Attrib_Intensity(bool value)
{
    Cursor.intensity = value ;
    if (value) {
        if (Cursor.reverse)
            Cursor.background_colour += Cursor.background_colour < 8 ? 8 : 0;
        else
            Cursor.foreground_colour += Cursor.foreground_colour < 8 ? 8 : 0;
    } else {
        if (Cursor.reverse)
            Cursor.background_colour -= Cursor.background_colour < 16 ? 8 : 0;
        else
            Cursor.foreground_colour -= Cursor.foreground_colour < 16 ? 8 : 0;
    }
}
#endif
/**
 * Set Attribute Flags
 * 
 * \param value Flag
 * \return none
 */
void Set_Attrib_Flags(uint8_t value)
{
    Cursor.Flags = value ;
}