/**
 * BSD 3-Clause License
 * 
 * Copyright (c) 2023, DarkElvenAngel
 * All rights reserved.
 * 
 * see LICENCE for full Licence
 */
// Dual Mode Text and Graphics

#include <stdio.h>
#include <stdarg.h>
#include "pico.h"
#include "pico/stdlib.h"
#include "pico/scanvideo.h"
#include "pico/scanvideo/composable_scanline.h"
#include "pico/multicore.h"
#include "hardware/irq.h"
#include "font.h"
#include "charbuffer_improved.h"
#include "config.h"
#if defined (CHARBUFFER_SUPPORT_COLOUR_16) || defined (CHARBUFFER_SUPPORT_COLOUR_256)
#include "pallet.h"
#endif
#include <string.h>

typedef volatile enum {
    MODE_TXT,       // 28 lines of text with 2 lines used for Header and Foot TBD
    MODE_T2,        // 80 col 25 lines of text with 16 foreground and 16 background colours 
    MODE_G0,        // High Resolution Monochrome Mode 640 x 200 or 320 x 200 
    MODE_G1,        // 4   Colour Mode   320 x 200 user defined pallet  2bpp
    MODE_G2,        // 16   Colour Mode  320 x 100 user defined pallet  4bpp
    MODE_G3,        // 256 Colour Mode   160 x 100 user defined pallet  8bpp
    MODE_G4,        // UNDEFINED
} MODE;


static void Init_Sprites(){
    memset(_VBUFFERS.SPRITE_BUFF,0,sizeof(_VBUFFERS.SPRITE_BUFF));
    memcpy(_VBUFFERS.SPRITE_CLUT,Pallets[1],sizeof(uint16_t) * 15);
    memset(_VBUFFERS.SPRITE_REGS,0, 128);  // This should be a define!
    return;
} 


MODE _Display_Mode = MODE_TXT;
bool Cursor_Blink = true;
bool _Blink = 0;
#ifndef led_state
extern bool led_state;
#else
bool led_state = true;
#endif

char HEADER_BUFFER[CHARBUFFER_COLUMNS] = "";
char FOOTER_BUFFER[CHARBUFFER_COLUMNS] = "";

void CLR_HEADER(){ memset(HEADER_BUFFER, 0, MAX_COL); }
void CLR_FOOTER(){ memset(FOOTER_BUFFER, 0, MAX_COL); }
void Set_HEADER(const char *format, ... )
{
    CLR_HEADER();
    va_list args;
    va_start(args, format);
    vsnprintf(HEADER_BUFFER,MAX_COL,format, args);
    va_end(args);
}
void Set_FOOTER(const char *format, ... )
{
    CLR_FOOTER();
    va_list args;
    va_start(args, format);
    vsnprintf(FOOTER_BUFFER,MAX_COL,format, args);
    va_end(args);
}
void Set_Display_Mode(uint8_t m)
{
    _Display_Mode = m ;
}
bool Blink_task(struct repeating_timer *t)
{
    if (Cursor_Blink) _Blink -= 1; else _Blink = 0;
    return true;
}

/**
 * Set Cursor Visiblity
 * 
 * \param visiblity on or off
 * \return none
 */
void Set_Cursor(bool visiblity)
{
    Cursor_Blink = visiblity;
    if (!visiblity) _Blink = 0;
}
scanvideo_mode_t vga_text_mode =
{
    .default_timing = &vga_timing_640x480_60_default,
    .pio_program = &video_24mhz_composable,
    .width = 640,
    .height = 480,
    .xscale = VGA_X_SCALING,
    .yscale = VGA_Y_SCALING,
};

// Ultra High Res Mode 640x200 1bpp
void __time_critical_func(draw_grid_uhr)(int y, uint16_t *pixel_pointer){
        uint8_t c = 15;
        uint8_t b = 0;
        for ( int Current_Column = 0; Current_Column < 80; Current_Column++ ) 
        {
            uint8_t bits = _VBUFFERS.VIDEO_BUFF[Current_Column + (y * 80)] ;
                *pixel_pointer = ( bits & 0x80 ) ? _VBUFFERS.CLUT[c] : _VBUFFERS.CLUT[b];
                ++pixel_pointer;
                *pixel_pointer = ( bits & 0x40 ) ? _VBUFFERS.CLUT[c] : _VBUFFERS.CLUT[b];
                ++pixel_pointer;
                *pixel_pointer = ( bits & 0x20 ) ? _VBUFFERS.CLUT[c] : _VBUFFERS.CLUT[b];
                ++pixel_pointer;
                *pixel_pointer = ( bits & 0x10 ) ? _VBUFFERS.CLUT[c] : _VBUFFERS.CLUT[b];
                ++pixel_pointer;
                *pixel_pointer = ( bits & 0x08 ) ? _VBUFFERS.CLUT[c] : _VBUFFERS.CLUT[b];
                ++pixel_pointer;
                *pixel_pointer = ( bits & 0x04 ) ? _VBUFFERS.CLUT[c] : _VBUFFERS.CLUT[b];
                ++pixel_pointer;
                *pixel_pointer = ( bits & 0x02 ) ? _VBUFFERS.CLUT[c] : _VBUFFERS.CLUT[b];
                ++pixel_pointer;
                *pixel_pointer = ( bits & 0x01 ) ? _VBUFFERS.CLUT[c] : _VBUFFERS.CLUT[b];
                ++pixel_pointer;
        }
}
// High Res Mode 320x200 2bpp

void __time_critical_func(draw_grid_hr)(int y, uint16_t *pixel_pointer){
        for ( int Current_Column = 0; Current_Column < 80; ++Current_Column ) 
        {
            uint8_t bits = _VBUFFERS.VIDEO_BUFF[Current_Column + (y * 80)] ;
                *pixel_pointer = _VBUFFERS.CLUT[(bits >> 6) & 0x3 ];
                ++pixel_pointer;
                *pixel_pointer = _VBUFFERS.CLUT[(bits >> 6) & 0x3 ];
                ++pixel_pointer;

                *pixel_pointer = _VBUFFERS.CLUT[(bits >> 4) & 0x3 ];
                ++pixel_pointer;
                *pixel_pointer = _VBUFFERS.CLUT[(bits >> 4) & 0x3 ];
                ++pixel_pointer;
                *pixel_pointer = _VBUFFERS.CLUT[(bits >> 2) & 0x3 ];
                ++pixel_pointer;
                *pixel_pointer = _VBUFFERS.CLUT[(bits >> 2) & 0x3 ];
                ++pixel_pointer;
                *pixel_pointer = _VBUFFERS.CLUT[(bits >> 0) & 0x3 ];
                ++pixel_pointer;
                *pixel_pointer = _VBUFFERS.CLUT[(bits >> 0) & 0x3 ];
                ++pixel_pointer;
            
        }
}

// 320x100 x 16 colour mode
void __time_critical_func(draw_grid)(int line_num, uint16_t *pixel_pointer){

        uint8_t y = line_num / 2;
        uint8_t c = 15;
        for ( int Current_Column = 0; Current_Column < 160; ++Current_Column ) 
        {
            uint8_t bits = _VBUFFERS.VIDEO_BUFF[Current_Column + (y * 160)] ;
                *pixel_pointer = _VBUFFERS.CLUT[(bits >> 4) & 0xF];  
                ++pixel_pointer;
                *pixel_pointer = _VBUFFERS.CLUT[(bits >> 4) & 0xF]; 
                ++pixel_pointer;

                *pixel_pointer = _VBUFFERS.CLUT[bits & 0xF];
                ++pixel_pointer;
                *pixel_pointer = _VBUFFERS.CLUT[bits & 0xF];
                ++pixel_pointer; 
            
         }
}
// 160x100 x 256 colour mode
void __time_critical_func(draw_G3)(int line_num, uint16_t *pixel_pointer){

        uint8_t y = line_num / 2;
        for ( int Current_Column = 0; Current_Column < 160; ++Current_Column ) 
        {
            //if (++c > 15) c = 0;
            uint8_t c = _VBUFFERS.VIDEO_BUFF[Current_Column + (y * 160)] ;
                *pixel_pointer = _VBUFFERS.CLUT[c];
                ++pixel_pointer;
                *pixel_pointer = _VBUFFERS.CLUT[c];
                ++pixel_pointer;
                *pixel_pointer = _VBUFFERS.CLUT[c];
                ++pixel_pointer;
                *pixel_pointer = _VBUFFERS.CLUT[c];
                ++pixel_pointer;
         }
}

#define GLYPH_HEIGHT_MASK 7

void
__time_critical_func(draw_text2)(int line_num, uint16_t *pixel_pointer) {
  uint16_t y_vid = (uint16_t)(line_num / GLYPH_HEIGHT);
  uint16_t y_fnt = (uint16_t)(line_num & GLYPH_HEIGHT_MASK);

  const uint8_t *v_buf = &_VBUFFERS.VIDEO_BUFF[y_vid * 160];
  for (int x = 0; x < 80; x++) {
    uint8_t color     = *v_buf++;
    uint8_t character = *v_buf++;

    uint16_t fg = _VBUFFERS.CLUT[color & 0xf];
    uint16_t bg = _VBUFFERS.CLUT[(color >> 4) + 0x10];
    uint8_t  bits = _VBUFFERS.FONT_BUFFER[character * GLYPH_HEIGHT + y_fnt];

    for (uint8_t mask = 128; mask > 0; mask >>= 1) {
      *pixel_pointer++ = (bits & mask) ? fg : bg;
    }
  }
}
void __time_critical_func(draw_cursor)(int line_num, uint16_t *pixel_pointer){
    if (!_Blink) return;
    if (line_num != (Cursor.Line * GLYPH_HEIGHT) + 7) return;
 
    pixel_pointer += GLYPH_WIDTH * Cursor.Column;
    for (uint8_t i = 0; i < 8; i++)
    {
        *pixel_pointer = _VBUFFERS.CLUT[15];
        ++pixel_pointer;
    }
}
void __time_critical_func(draw_sprites)(int Line_y, uint16_t *p){
    uint16_t *ps = p;

    for (uint8_t S = 0; S < 16; S++){
        int y = Line_y;
        _SPRITE_HEADER_T* Sprites = _VBUFFERS.SPRITE_HEADERS;
        if (Sprites[S].data_length == 0) continue;
        if (Sprites[S].scale_y) y = y/2;
        if (y >= Sprites[S].y && y < Sprites[S].y + Sprites[S].h)
        {
            p = ps;
            int16_t PP = Sprites[S].x;
            p += Sprites[S].x;
            switch (Sprites[S].colour_depth)
            {
                case 0:{  // 1BPP 
                uint16_t DY = (y - Sprites[S].y) * (Sprites[S].w / 8) + Sprites[S].data_offset ;
                for (uint8_t i = 0; i < (Sprites[S].w  ); i++)
                {
                    if (PP < 0 || PP++ > 640) { continue;}
                    uint8_t B = i/8;
                    uint8_t Bit = 7-(i%8);
                    uint8_t CI = (*(_VBUFFERS.SPRITE_BUFF + DY + B) >> Bit) & 1;
                    if (CI != 0) *p = _VBUFFERS.SPRITE_CLUT[CI + Sprites[S].pallet_shift];
                    else if (Sprites[S].use_all_colours) *p = _VBUFFERS.SPRITE_CLUT[CI] ;
                    ++p;
                    if (Sprites[S].scale_x) {
                        if (PP < 0 || PP++ > 640) { continue;}
                        if (CI != 0) *p = _VBUFFERS.SPRITE_CLUT[CI + Sprites[S].pallet_shift];
                        else if (Sprites[S].use_all_colours) *p = _VBUFFERS.SPRITE_CLUT[CI] ;
                        ++p;
                    }
                }
                } break;
                case 1:{  // 2BPP
                uint16_t DY = (y - Sprites[S].y) * (Sprites[S].w / 4) + Sprites[S].data_offset ;
                for (uint8_t i = 0; i < (Sprites[S].w  ); i++)
                {
                    if (PP < 0 || PP++ > 640) { continue;}
                    uint8_t B = i/4;
                    uint8_t Bit = 6-(i%4*2);
                    uint8_t CI = (*(_VBUFFERS.SPRITE_BUFF + DY + B) >> Bit) & 3; CI += Sprites[S].pallet_shift;
                    if (CI != 0) *p = _VBUFFERS.SPRITE_CLUT[CI + Sprites[S].pallet_shift];
                    else if (Sprites[S].use_all_colours) *p = _VBUFFERS.SPRITE_CLUT[CI] ;
                    ++p;
                    if (Sprites[S].scale_x) {
                        if (PP < 0 || PP++ > 640) { continue;}
                        if (CI != 0) *p = _VBUFFERS.SPRITE_CLUT[CI + Sprites[S].pallet_shift];
                        else if (Sprites[S].use_all_colours) *p = _VBUFFERS.SPRITE_CLUT[CI] ;
                        ++p;
                    }
                }
                } break;
                case 2:{  // 4BPP
                uint16_t DY = (y - Sprites[S].y) * (Sprites[S].w / 2) + Sprites[S].data_offset ;
                for (uint8_t i = 0; i < (Sprites[S].w  ); i++)
                {
                    if (PP < 0 || PP++ > 640) { continue;}
                    uint8_t B = i/2;
                    uint8_t Bit = 4-(i%2*4);
                    uint8_t CI = (*(_VBUFFERS.SPRITE_BUFF + DY + B) >> Bit) & 15; CI += Sprites[S].pallet_shift;
                    if (CI != 0) *p = _VBUFFERS.SPRITE_CLUT[CI + Sprites[S].pallet_shift];
                    else if (Sprites[S].use_all_colours) *p = _VBUFFERS.SPRITE_CLUT[CI] ;
                    ++p;
                    if (Sprites[S].scale_x) {
                        if (PP < 0 || PP++ > 640) { continue;}
                        if (CI != 0) *p = _VBUFFERS.SPRITE_CLUT[CI + Sprites[S].pallet_shift];
                        else if (Sprites[S].use_all_colours) *p = _VBUFFERS.SPRITE_CLUT[CI] ;
                        ++p;
                    }
                }
                }break;
                default:{ // 8BPP
                uint16_t DY = (y - Sprites[S].y) * (Sprites[S].w) + Sprites[S].data_offset ;
                for (uint8_t i = 0; i < (Sprites[S].w  ); i++)
                {
                    if (PP < 0 || PP++ > 640) { continue;}
                    uint8_t CI = (*(_VBUFFERS.SPRITE_BUFF + DY + i) );
                    if (Sprites[S].use_all_colours) *p = _VBUFFERS.SPRITE_CLUT[CI] ;
                    else if (CI != 0) *p = _VBUFFERS.SPRITE_CLUT[CI] ;
                    ++p;
                    if (Sprites[S].scale_x) {
                        if (PP < 0 || PP++ > 640) { continue;}
                        if (Sprites[S].use_all_colours) *p = _VBUFFERS.SPRITE_CLUT[CI] ;
                        else if (CI != 0) *p = _VBUFFERS.SPRITE_CLUT[CI] ;
                        ++p;
                    }
                }
                }break;
            }
        }
    }
}
void (*RENDER_FUNCS[6])(int,uint16_t*) = 
{ draw_text2, draw_text2, draw_grid_uhr, draw_grid_hr, draw_grid, draw_G3 };
void __time_critical_func(render_loop_G) ()
{
    uint8_t t = 0;
    while (_Display_Mode != MODE_TXT)
    {
        scanvideo_scanline_buffer_t *scanline_buffer = scanvideo_begin_scanline_generation(true);
        int Scanline = scanvideo_scanline_number (scanline_buffer->scanline_id);
        uint16_t *pixels = (uint16_t *) (scanline_buffer->data);
        if (Scanline < 20 || Scanline > 219)
        {
            scanline_buffer->data[0] = COMPOSABLE_RAW_1P;
            scanline_buffer->data[1] = COMPOSABLE_EOL_SKIP_ALIGN;
            scanline_buffer->data_used = 2;
        } else {
            Scanline -= 20;
            pixels += 2;   
            if (_Display_Mode == MODE_G3)draw_G3(Scanline,pixels);//(scanline_buffer);
            if (_Display_Mode == MODE_G2)draw_grid(Scanline,pixels);//(scanline_buffer);
            if (_Display_Mode == MODE_G1)draw_grid_hr(Scanline,pixels);//(scanline_buffer);
            if (_Display_Mode == MODE_G0)draw_grid_uhr(Scanline,pixels);//(scanline_buffer);
            if (_Display_Mode == MODE_T2){draw_text2(Scanline,pixels);draw_cursor(Scanline,pixels);}//(scanline_buffer);
            /* (*RENDER_FUNCS[_Display_Mode])(Scanline,pixels);
            if (_Display_Mode == MODE_T2)draw_cursor(Scanline,pixels); */
            draw_sprites(Scanline,pixels); //scanline_buffer);
            pixels += 640;
            *pixels = 0;
            ++pixels;
            *pixels = COMPOSABLE_EOL_ALIGN;
            pixels = (uint16_t *) scanline_buffer->data;
            pixels[0] = COMPOSABLE_RAW_RUN;
            pixels[1] = pixels[2];
            pixels[2] = 645; //(MAX_COL * GLYPH_WIDTH - 2);
            scanline_buffer->data_used = ( MAX_COL * GLYPH_WIDTH + 4 ) / 2;
        }

        scanvideo_end_scanline_generation(scanline_buffer);
    }
}
void __time_critical_func(render_loop_T) ()
{
    byte *Font = font_reg;
    while (_Display_Mode == MODE_TXT)
    {
        struct scanvideo_scanline_buffer *buffer = scanvideo_begin_scanline_generation (true);
        uint16_t *pixel_pointer = (uint16_t *) buffer->data;
        int Current_Line = (uint16_t)buffer->scanline_id / GLYPH_HEIGHT;
        uint16_t Scanline = (uint16_t)buffer->scanline_id - (GLYPH_HEIGHT * Current_Line);
#if VGA_FIRST_LINE > 0
        Current_Line -= VGA_FIRST_LINE ;
#endif
        pixel_pointer += 1;
        if (Current_Line  < 0 || Current_Line >= MAX_LINE)
        {
            if (Current_Line == -1 || Current_Line == MAX_LINE) {
                uint16_t fg = 0xFFDF;
                uint16_t bg = 0;//xFFDF;// 0x2BF;
                uint8_t grad = 0;
                for ( int Current_Column = 0; Current_Column < MAX_COL; ++Current_Column ) 
                {
                    uint8_t character = 0;
                    character = Current_Line == -1 ? HEADER_BUFFER[Current_Column] : FOOTER_BUFFER[Current_Column];
                    uint8_t bits = Font[8 * character + Scanline];
                    for (uint8_t Bit_ptr = 0x80; Bit_ptr > 0; Bit_ptr = Bit_ptr >> 1)
                    {
                        ++pixel_pointer;
                        *pixel_pointer = ( bits & Bit_ptr ) ? fg : bg;
                    }
                    // if (++grad == 3) { bg-=0x841; grad=0; }
                }
                ++pixel_pointer;
                *pixel_pointer = 0;
                *pixel_pointer = COMPOSABLE_EOL_ALIGN;
                pixel_pointer = (uint16_t *) buffer->data;
                pixel_pointer[0] = COMPOSABLE_RAW_RUN;
                pixel_pointer[1] = pixel_pointer[2];
                pixel_pointer[2] = MAX_COL * GLYPH_WIDTH - 2;
                buffer->data_used = ( MAX_COL * GLYPH_WIDTH + 4 ) / 2;
            } else {
                buffer->data[0] = COMPOSABLE_RAW_1P;
                buffer->data[1] = COMPOSABLE_EOL_SKIP_ALIGN;
                buffer->data_used = 2; 
            }
        } else {
        for ( int Current_Column = 0; Current_Column < MAX_COL; ++Current_Column ) 
        {
            uint8_t character = 0;
#ifdef SIMPLE_CHARBUFFER        // No Attributes or colour
            if (Current_Line < MAX_LINE) character = char_buffer[Current_Line][Current_Column];
            uint16_t fg = 0x2BF;
            uint16_t bg = 0;
#endif
#ifdef FULL_CHARBUFFER        // Attributes enabled
            if (Current_Line < MAX_LINE) character = char_buffer[Current_Line][Current_Column].character;
            Font = (char_buffer[Current_Line][Current_Column].bold) ? font_bold : font_reg;
            if (char_buffer[Current_Line][Current_Column].extended)
            {
                Font = USR_FONT;
            }
#if defined(CHARBUFFER_SUPPORT_COLOUR_16)
            uint16_t fg = Pallets[Cursor.Pallet][char_buffer[Current_Line][Current_Column].foreground_colour];
            uint16_t bg = Pallets[Cursor.Pallet][char_buffer[Current_Line][Current_Column].background_colour];

#elif defined(CHARBUFFER_SUPPORT_COLOUR_256)
            uint16_t fg = Pallets_256[Cursor.Pallet & 1][char_buffer[Current_Line][Current_Column].foreground_colour];
            uint16_t bg = Pallets_256[Cursor.Pallet & 1][char_buffer[Current_Line][Current_Column].background_colour];
#else
            uint16_t fg = 0x7FFF;;
            uint16_t bg = 0;
#endif
#endif
            uint8_t bits = Font[8 * character + Scanline];
#ifdef FULL_CHARBUFFER        // Attributes enabled
            if (char_buffer[Current_Line][Current_Column].underline && Scanline == 7) bits = 0xff;
#endif
#if !defined(CHARBUFFER_SUPPORT_COLOUR_16) && !defined(CHARBUFFER_SUPPORT_COLOUR_256)
            if (Cursor.Line == Current_Line && Cursor.Column == Current_Column && Scanline == 7 && led_state) { bits = 0xff; fg = 0x7FFF; }
#else
            if (Cursor.Line == Current_Line && Cursor.Column == Current_Column && Scanline == 7 && _Blink) { bits = 0xff; fg = Pallets[Cursor.Pallet][15]; }
#endif
            for (uint8_t Bit_ptr = 0x80; Bit_ptr > 0; Bit_ptr = Bit_ptr >> 1)
            {
                ++pixel_pointer;
                *pixel_pointer = ( bits & Bit_ptr ) ? fg : bg;
            }
        }
        ++pixel_pointer;
        *pixel_pointer = 0;
        *pixel_pointer = COMPOSABLE_EOL_ALIGN;
        pixel_pointer = (uint16_t *) buffer->data;
        pixel_pointer[0] = COMPOSABLE_RAW_RUN;
        pixel_pointer[1] = pixel_pointer[2];
        pixel_pointer[2] = MAX_COL * GLYPH_WIDTH - 2;
        buffer->data_used = ( MAX_COL * GLYPH_WIDTH + 4 ) / 2;
        }
        scanvideo_end_scanline_generation (buffer);
    }
}
void __time_critical_func(render_loop) ()
{
    while (1)
    {
        switch (_Display_Mode) {
            case MODE_TXT:  render_loop_T();    break;
            default:        render_loop_G();    break;
        }
    }
}
//static uint32_t VIDEO_STACK[1024];
void video_init ()
{
    Init_Sprites();
    // Initialize the CLUT with the CGA Text Pallet.
    memcpy(_VBUFFERS.CLUT,Pallets_256[0],sizeof(uint16_t) * 256);
    // Initialize the CLUT with the CGA Text Pallet.
    memcpy(_VBUFFERS.CLUT,Pallets[0],sizeof(uint16_t) * 16);
    FNTBUFF_init();
    scanvideo_setup(&vga_text_mode);
    scanvideo_timing_enable(true);
    multicore_launch_core1(render_loop);
}

