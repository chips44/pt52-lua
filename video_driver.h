#ifndef VIDEO_DRIVER_H
#define VIDEO_DRIVER_H

void video_init (void);
bool Blink_task(struct repeating_timer *t);
typedef volatile enum {
    MODE_TXT,       // 28 line of text with 2 lines used for Header and Foot TBD
    MODE_T2,
    MODE_G0,        // High Resolution Monochrome Mode 640 x 200 or 320 x 200 
    MODE_G1,        // 4   Colour Mode   320 x 200 user defined pallet  2bpp
    MODE_G2,        // 16  Colour Mode   320 x 100 user defined pallet  8bpp
    MODE_G3,        // 256 Colour Mode   160 x 100 user defined pallet 16bpp
    MODE_TILE,      // Tile graphical mode 320 x 200 with 250 8x8 tiles a 4bpp
    MODE_BLK,       // BLANK SCREEN NO RENDER
} MODE;

//extern MODE _Display_Mode ;
#define _Display_Mode _VBUFFERS.VIDEO_REG._Display_Mode

/**
 * Set Cursor Visiblity
 * 
 * \param visiblity on or off
 * \return none
 */
void Set_Cursor(bool visiblity);
void Set_Display_Mode(uint8_t m);
#endif