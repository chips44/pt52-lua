# PT52-Lua Libraries

## OS Library *MODIFICATIONS*

The default os library has been modified for the PT52

- execute() - Is not available
- setenv(*NAME*, *VALUE*) - set an environment variable **NAME** to **VALUE**
- unsetenv(*NAME*) - unset an environment variable **NAME**

## IO Library *MODIFICATIONS*

The default io library has been modified for the PT52

## PT Library

- resetscreen() - Reset the screen clear the buffer
- clr() - Clear screen only
- clearline(mode) - Clear the current line
- cursorto(Row,Column) - Move the cursor to Row column
- putchar_free(Row,Column,Char) -
- putchar(Char) -
- cursor_save() - Save the cursor location
- cursor_restore() - Restore cursor to the last saved location
- set_bg(c) - set the current background colour
- set_fg(c) - set the current foreground colour
- set_colours() - set colour in the lookup table
- set_pallet(n) - load pallet n
- set_exteneded() -
- set_header(string) - set the screen header (Only in Text mode 1)
- set_footer(string) - set Screen Footer (Only in Text mode 1)
- set_textmode(mode) - Switch Text mode
- use_fontset() -
- load_font() -
- getchar() -
- get_keycode() -
- sleep(x) - Sleep for x *BLOCKING*
- memlist() - Do not use
- `alarm(t,[function])` - fire [function] after t * 100 ms  see example
- `key_report()` -
- `set_key_handle()` -
- `clr_key_handle()` -

Source File [lptlib.c](programs/lua/lptlib.c)

### Alarm Example

Call function after 1 second

```lua
uses("pt")
function delayed()
  print("Times up!")
end

pt.alarm(10, delayed)
```

Fire a function every 100ms

```lua
uses("pt")
pt.alarm(1, function() print("tick") pt.alarm(1) end)
```

---

## DRAW Library

- clr() - Clear the screen
- line() - Draw a line
- rect() - Draw a Rectangle
- roundrect() - Draw a rounded rectangle
- circle() - Draw a circle
- ellipse() - Draw an ellipse
- pixel() - paint a pixel
- mode() - Set the graphics mode
- write() -
- vpoke() - read from frame buffer memory
- vpeek() - write to frame buffer memory
- char() - write a character to the screen
- string() - write a string to the screen
- set_colour() - set colour in the CLUT
- pallet() - set current pallet

Source File [ldrawlib.c](programs/lua/ldrawlib.c)

---

## SPRITES Library

- `pos()` - set position of the sprite
- `size()` - set size
- `length()` - set how much data is used to draw sprite
- `offset()` - set offset where data for current sprite starts
- `depth()` - set how many colours  
- `transparent()` - set if colour 0 is used or not
- `move()` - move sprite
- `set_scaling()` - a sprite can be pixel doubled in x and y
- `shift_pallet()` -
- `get_pos()` - return position of sprite
- `vpoke()` - write to sprite buffer
- `vpeek()` - read form sprite buffer
- `set_colour()` - set colour in the sprite CLUT
- `pallet()` -  load a pallet to teh sprite CLUT

Source File [lspriteslib.c](programs/lua/lspriteslib.c)
  
---

## VRAM Library

- `read([where], offset, [length])`  
- `write([where], offset, data)`  
- `search({address}, Char, Length)`
- `compare({address_1},{address_2}, Length)`
- `copy({address_to},{address_from}, Length)`
- `move({address_to},{address_from}, Length)`
- `set({address}, Data, Length, [numBytes])`
- `load({address}, Filename, Length)`
- `save({address}, Filename, Length)`  
- `fetch` **under consideration**
- `store` **under consideration**

`[where]`  
TYPE : String
Accepted values : "all", "frame", "reg", "clut", "pclut", "sreg", "sprite", "font", "xfont"  
Default : all  

Set the read/write base address and access limit to a range of the VRAM.

`offset`  
TYPE : Number  
Accepted values : 0 - 23552  
Default : 0  

Sets offset from the chosen base address to access.

`{address}` alias of `[where], offset`  
NOTE : address is alignment checked for writes of numbers larger than 255

`data`  
TYPE : NUMBER or TABLE  
Accepted values : 0 - 255  
Default : 0  

Value(s) to be written to the memory address pointed to.

`length`  
TYPE : NUMBER  
Accepted values : 1 - 23551  
Default : 1

How much data to read if `length` > 1 a table is returned.

`[numBytes]`  
TYPE : NUMBER  
Accepted values : 1 , 2,  or 4  
Default : 1

This sets the size of the data to be used.

`Filename`  
TYPE : STRING  

This is the working file's name that you want to read from or to.

Source File [lrvamlib.c](programs/lua/lvramlib.c)

## FS Library

- `chdir(path)` - Change current working directory
- `mkdir(path)` - Make directory
- `rmdir(path)` - Remove directory only if it's empty
- `stat(file)` - Check if file/dir exists and return file obj
- `pwd()` - return string of current working directory
- `dir(path, [pattern])` - returns a DIR\* object used to list files in path optionally pattern can be set default is "\*"
  
### Constants for attributes

- `AM_RDO` - readonly
- `AM_HID` - hidden
- `AM_SYS` - system / exec
- `AM_DIR` - directory
- `AM_ARC` - archive

### DIR\* Object

- `{DIR*}:close()` - close the DIR\*
- `{DIR*}:rew()` - Rewind DIR\* to the top of the list
- `{DIR*}:next()` - Manual iterate to the next entry returns file obj

Note: DIR\* can be used in for loops see example

### file obj

- `{file obj}` - return file name
- `{file obj}.name` - return file name
- `{file obj}.size` - return file size
- `{file obj}.attrib` - return attrib flags (integer)

### FS Library Example

```lua
uses("fs")
d = fs.dir(".")
for f in d do
  print(f, f.size, f.attrib)
end
```

Source File [lfslib.c](programs/lua/lfslib.c)

## UART Library

- init([baud_rate], [data_bits], [stop_bits], [parity]) - Initialize the UART
- close() - Close the UART
- setup([baud_rate], [data_bits], [stop_bits], [parity]) - Setup UART configuration
- send(data) - Send data. Data can be a string or number
- translate_crlf(boolean) - Set CR/LF conversion on UART
- isReadable() - Check if data is waiting in the RX FIFO
- receive() - Receive data
- setBaudRate(BaudRate) - Change baud rate
- getConfig() - Get current configuration
- setFIFO(boolean) - Set fifo on or off

Source File [luartlib.c](programs/lua/luartlib.c)

## OVERLAY Library

- reset() - Reset the overlay to default doesn't enable or disable
- clr([fill_colour]) - fill the overlay with a colour 0 - 15. 0 default
- line(x0, y0, x1, y1, colour) - draw a line
- rect(x,y,w,h,colour, [fill_colour]) - draw a rectangle
- roundrect(x,y,w,h,r,colour) - draw a rounded rectangle
- circle(x,y,r,c,[fill_colour]) - draw a circle
- ellipse(x,y,rx,ry,colour) - draw an ellipse
- pixel(x, y, colour) - plot a pixel
- putchar(x, y, colour, font, character) - put at character
- print(x, y, colour, string) - put a string
- set_colour(colour_index, r, g, b) - Define new colour in OVERLAY_CLUT
- pallet(value) - Load a Pallet into OVERLAY_CLUT
- reg(register, [value]) - Read/Write overlay register

The Overlay has a fixed colour depth of 4bpp or 16 colours.

When `Fill_colour` isn't specified then no fill is used.

Overlay can map to more that one CLUT it's important to keep in mind that the `set_colour` and `pallet` functions only work on the Overlay pallet not the CLUT that the overlay is using.

`reg` is a special function that lets you manipulate the overlay registers with ease.  the registers are:

- x - The X position of the overlay
- y - The Y position of the overlay
- w - Width
- h - Height
- clut - Colour Lookup Table 0: default, 1: Main CLUT, 2: Sprite CLUT
- scale x - pixel double in X axis
- scale y - pixel double in Y axis
- enable - Enable overlay this disables sprites
- transparent - Colour 0 of the overlay is transparent
- protected - Overlay memory is protected from system writes
- window - When in Main CLUT mode the starting colour index to use

The `print` function has a mini ESC sequence parser and parses `\n`, `\r`, `\t` it also supports line wrap and keeps text aligned on newline. `print` auto switches to icon mode when any extended ASCII character is use > 127.

The escape sequences are very simple:

- [ESC]~(0-G) - set the text colour to 0 - 15(F) G resets to default. **CAPITALS ONLY**
- [ESC]#(0-3) - set the font used.  0: 6x8 1: Normal 2: BOLD 3: Code Page 437

***NOTE*** *The overlay is a framebuffer meaning that font glyphs are just bitmap copies to the the buffer and are treated as transparent always*

Source File [loverlaylib.c](programs/lua/loverlaylib.c)
