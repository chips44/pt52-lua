/**
 * BSD 3-Clause License
 * 
 * Copyright (c) 2021, DarkElvenAngel
 * All rights reserved.
 * 
 * see LICENCE for full Licence
 */
// VT52 Digital Terminal Escape Codes
// Process Keyboard to send correct VT52 output
#include "config.h"
#include "pt52io.h"
#include <string.h>
#include <stdlib.h>

/**
 * @brief VT100 and PT52 ESC Processors
 * 
 * 1 Act as a passive display, implement the 4 cursor commands, the 2 erase commands, direct cursor addressing, and at least inverse characters.
 * 2 To enter data in VT100 mode, implement the 4 cursor keys and the 4 PF keys. It must be possible to enter ESC, TAB, BS, DEL, and LF from the keyboard.
 * 3 For doing full-screen editing in VT100, implement directed erase, the numeric keypad in applications mode, and the limited scrolling region.
 * 4 If the hardware is capable of double width/double height we will again try to implement this
 * 5 Colour support
 * 
 */


#ifdef ENABLE_VT100
#define ESC_INDEX_LIMIT 8

/* // ESC Status
enum eESC_mode
{
    ESC_OFF     = 0,
    ESC_ONLY    = 1,    // ESC
    ESC_CSI     = 2,    // ESC [
    ESC_CSI_Q   = 3,    // ESC [?
    // ESC_CONF    = 4,    // ESC [ ... {h|l} 
    ESC_G0      = 5,    // ESC (
    ESC_G1      = 6,    // ESC )
    ESC_AMB     = 7,    // ESC #
    ESC_DCS     = 8,    // ESC P
}; */
// int ESC_Params[ESC_INDEX_LIMIT];

void  _ESC_ONLY  (char ch);
void  _ESC_CSI   (char ch);
void  _ESC_CSI_Q (char ch);
void  _ESC_G0    (char ch);
void  _ESC_G1    (char ch);
void  _ESC_AMB   (char ch);
void  _ESC_DCS   (char ch);
bool  _ESC_DECODE(char ch);

enum eESC_mode ESC_Mode = ESC_OFF;
int ESC_Params[ESC_INDEX_LIMIT];
bool Lamps [4] = { 0 };

uint8_t Param_index;

void _ESC_RESET()
{
    ESC_Mode = ESC_OFF;
    Param_index = 0;
    memset(ESC_Params, 0, sizeof(ESC_Params));
}

#endif

bool _Prase_ESC(char data)
{
    // char_buffer[Cursor.Line][Cursor.Column++] = (data + 0x100);
    // Cursor_Position_Check();
    static bool vt52_Y = false;
    static char More = 0;
    static uint8_t count = 0;
    static uint8_t params[2] = { 0 };
    if (data < 0x20 || data == 0x7F)                    // These are control characters and we want to display them
    {
        Put_char(data);
        return false;
    }
    if (More) { switch (More)
    {
        case 'Y' : // ESC Y [line] + 040 [column] + 040
            params[count++] = data - 32;
            if (count == 2) // COMPLETE
            {
                Move_cursorto(params[1],params[0]);
                count = 0;
                break;
            }
            return true;
        case 'b' : // ESC b [colour] + 32
            if (count == 0 && (data - 32) < 16)
            {
                Set_BColour(data - 32);
                break;
            }
            params[count] = data;
            if (count == 1)
            {
                Set_BColour((uint16_t)strtoul((const char *)&params,NULL,16));
                count = 0;
                break;
            }
            count++;
            return true;// GET HEX COLOUR 
        case 'f' : // ESC f [colour] + 32
            if (count == 0 && (data - 32) < 16)
            {
                Set_FColour(data - 32);
                break;
            }
            params[count] = data;
            if (count == 1)
            {
                Set_FColour((uint16_t)strtoul((const char *)&params,NULL,16));
                count = 0;
                break;
            }
            count++;
            return true;// GET HEX COLOUR 
        case 'g' :
            Use_FontSet((data - 32) % MAX_FONTSETS);
            break;
        case 'p' : // ESC f [colour] + 32
            Set_Pallet(data - 32);
            break;
        case '_' : // custom character 
            if (count == 0)
            {
                params[0] = data - FONTSET_START ; 
                if (params[0] >= FONTSET_SIZE) return false;
                Clear_User_Char(data);
                count++;
                return 1; 
            }
            //params[(count % 2)] = data & 0x0f;                              // Save low nibble of data shift if params[0] selected
            //if (count % 2 == 0)
            //{
            //char_data[((count - 1)  / 2) + 1] |= ((data & 0x0f) << ( 8 * count % 2));            // Save byte to character data
            // data &= 0xf;
            data = strtol(&data,NULL,16);
            //data <<= 4 * (count % 2);
            USR_FONT[(params[0] * 8) +  ((count - 1)  / 2) ] |= data << (4 * (count % 2));//data;
            //char_data[((count - 1)  / 2) + 1] |= ((data & 0x0f) << ( 4 * count % 2));
            //}
            if (count == 16 )
            {
                count = 0;
                break;
            }
            count++;
            return true;
        case '[':
            if ((data >= '0' && data <= ';') || data == '?') return 1;
            break;
    }
    More = 0;
#if defined(ENABLE_VT100) && ENABLE_VT100 >= 1  
    _ESC_RESET();
#endif
    return false;
    }
    switch (data)
    {
#if defined(ENABLE_VT100) && ENABLE_VT100 >= 1      
    case '[':
        ESC_Mode = ESC_CSI;
        return true; // Important to return so we don't reset
    case '(':
        ESC_Mode = ESC_G0;
        return true; // Important to return so we don't reset
    case ')':
        ESC_Mode = ESC_G1;
        return true; // Important to return so we don't reset
    case '#':
        ESC_Mode = ESC_AMB;
        return true;
#endif       
    case 'A': Move_cursor(0,-1); break; // Up
    case 'B': Move_cursor(0, 1); break; // Down
    case 'C': Move_cursor(1, 1); break; // Right
    case 'D': Move_cursor(1,-1); break; // Left
    case 'T': 
        for (uint8_t L = 0; L < MAX_LINE; L++)
            for (uint8_t C = 0; C < MAX_COL; C++)
            {
#ifdef FULL_CHARBUFFER
                char_buffer[L][C].character = 'E';
#ifdef CHARBUFFER_SUPPORT_COLOUR_16
                char_buffer[L][C].colours = 0x0f;
#endif
#else
                char_buffer[L][C] = 'E';
#endif
            }
        break;
    case 'F': 
        Set_Attrib_Extended(true); 
        break;                           // Graphics Mode
    case 'G': 
        Set_Attrib_Extended(false);
        break;                          // Text Mode
    case 'g': More = data; return 1;    // Set Graphics Set
    case 'H': Home(0); break;           // Home
    case 'I': Reverse_Linefeed();break; // Reverse Linefeed
    case 'J': Clear_screen(0); break;   // Clear to end of screen
    case 'j': Clear_screen(2); break;   // Clear screen
    case 'K': Clear_line(0); break;     // Clear to end of line
    case 'k': Clear_line(2); break;     // Clear line
    case 'Y': More = data; return 1;    // Move to line column EXPECTS MORE INPUT
    case 'Z':                           // Report Terminal Type
#ifdef UART_ON
        uart_puts(UART_ID, "\e/K");     // Respond ESC / K  VT52[No-Printer]
#endif
        break;
    case '5': Set_Cursor(true); break;  // Show Cursor
    case '6': Set_Cursor(false); break; // Hide Cursor
    case '7': Save_cursor(); break;     // Save Cursor
    case '8': Restore_cursor(); break;  // Restore Cursor
    case '=': break;                    // Alternet Keypad mode on
    case '>': break;                    // Alternet Keypad mode off
    case 'b':                           // Set Background Colour EXPECTS MORE INPUT
    case 'f': More = data; return 1;    // Set Foreground Colour EXPECTS MORE INPUT
    case 'p': More = data; return 1;    // Set pallet EXPECTS MORE INPUT
    case 'd': Set_FColour(7); Set_BColour(0);  // Reset Colour and Attributes
    case 'c': Set_Attrib_Flags(0); break;
    case 'S': Set_Attrib_Bold(1);break;
    case 's': Set_Attrib_Bold(0);break;
    case 'R': Set_Attrib_Reverse(1);break;    // Reverse on
    case 'r': Set_Attrib_Reverse(0);break;    // Reverse off
    case 'U': Set_Attrib_Underline(1);break;    // Underline on
    case 'u': Set_Attrib_Underline(0);break;    // Underline off
    case '_': More = data; return 1;            // Define custom character
#if !defined(ENABLE_VT100) || ENABLE_VT100 == 0 
    case '[': More = data; return 1;    // CSI mode.
#endif
    case 'N': Buffer_Flags.Cr_Lf = 1; break; 
    case 'n': Buffer_Flags.Cr_Lf = 0; break; 
    default:

        break;
    }
#if defined(ENABLE_VT100) && ENABLE_VT100 >= 1  
    _ESC_RESET();
#endif
    return false;
}



#if defined(ENABLE_VT100) && ENABLE_VT100 >= 1
void _ESC_CSI(char ch)
{
    uint16_t x, y;
    switch (ch)
    {
    case '?':
        ESC_Mode = ESC_CSI_Q;
        return; // Important to return so we don't reset
    case '0' ... '9':
        ESC_Params[Param_index] = 10 * ESC_Params[Param_index] + (ch - '0');
        return; 
    case ';' :
        if (Param_index < ESC_INDEX_LIMIT) Param_index++;
        return;
    case 'A': // Cursor up
        if (ESC_Params[0] == 0) ESC_Params[0] = 1;
        Move_cursor(0,-ESC_Params[0]); break; // Up
        break; 
    case 'B': // Cursor down
        if (ESC_Params[0] == 0) ESC_Params[0] = 1;
        Move_cursor(0, ESC_Params[0]); break; // Down
        break; 
    case 'C':  // Cursor right
        if (ESC_Params[0] == 0) ESC_Params[0] = 1;
        Move_cursor(1, ESC_Params[0]); break; // Right
        break;
    case 'D':  // Cursor left
        if (ESC_Params[0] == 0) ESC_Params[0] = 1;
        Move_cursor(1,-ESC_Params[0]); break; // Left
        break;
    case 'E': break; // Move down 1 line and to first column.  ie <ENTER>
    case 'H': // ESC [ {line} ; {col} H|f  
    case 'f': // Move Cursor to line , col 
        if (( y = ESC_Params[0]) == 0) y = 1;
        if (( x = ESC_Params[1]) == 0) x = 1;
        // ORIGIN is 1, 1 Move_cursorto origin is 0,0
        Move_cursorto(x - 1,y - 1);
        break;
    case 'J': 
        /*switch (ESC_Params[0])
        {
            case 0: break; // Erase to end of screen from cursor
            case 1: break; // Erase to beginning of screen from cursor
            case 2: break; // Erase screen
        }*/
        Clear_screen(ESC_Params[0]);
        break;
    case 'K':
        /*switch (ESC_Params[0])
        {
            case 0: break; // Erase to end of line from cursor
            case 1: break; // Erase to beginning of line from cursor
            case 2: break; // Erase line 
        }*/
        Clear_line(ESC_Params[0]);
        break; // Erase to end of line
    case 'M': Reverse_Linefeed(); break; // Move Cursor up one line scroll back as needed
    case 'R':
        // This is a return code ESC [ R {line} ; {column} R
        // 0 indicates home position
        // The Position is relative to the Origin [origin mode (DECOM)]
        // We don't need to respond to this command.
        break;
    case 'c':  // What are you
        // Reply  "I'm BAT MAN!!!!!"
        // ESC [ ? 1 (; Ps) c
        // We'll always reply with Ps = 0 Base VT100, no options
#ifdef UART_ON
        uart_puts(UART_ID, "\e[?1;0c");
#endif
        break;
    case 'g': //Clear Htabs  0=current  3=ALL
#if 0
        switch (ESC_Params[0])
        {
            case 0: Clear_TabStop();break;
            case 3: Clear_TabStops(); break;
            default: break;
        }
#endif
        break;   
    case 'h':
    case 'l':
        break;
    case 'm': // colours and Attributes
#if 1 // REWRITE THIS MESS!!!!!!
        /* if (Param_index == 0) // ESC[m then reset
        {
            Set_Attrib_Reverse(0);
            Set_Attrib_Flags(0);
        } else  */
        for (int f = 0; f <= Param_index; f++) {
            /* if (ESC_Params[f] >= 30 && ESC_Params[f] <= 37)
            FG_Colour = (FG_Colour & 0x8 | ( ESC_Params[f] - 30)); // (reg_data & (~bit_mask)) | (new_value<<5)
            if (ESC_Params[f] >= 40 && ESC_Params[f] <= 47)
            BG_Colour = BG_Colour & 0x8 | ( ESC_Params[f] - 40); */
       
/*          Parameter	Parameter Meaning
            0	Attributes off
            1	Bold or increased intensity
            4	Underscore
            5	Blink
            7	Negative (reverse) image   */   
            switch (ESC_Params[f]) {
            case 0: // Attributes off
#if defined(VT100_USE_COLOUR) && VT100_USE_COLOUR == 1
                Set_FColour(7); Set_BColour(0);
#endif
                Set_Attrib_Reverse(0);
                Set_Attrib_Flags(0);
                break;
            case 1: // Bold or increased intensity
                Set_Attrib_Intensity(1); //Bold(1);
                break;
            case 4: // Underscore
                Set_Attrib_Underline(1);
                break;
            case 5: // Blink
                // unsupported
                break;
            case 7: // Negative (reverse) image
                Set_Attrib_Reverse(1);
                break; 
/// FONT SELECTION
            case 10 ... 13: // Primary Font
                Set_Attrib_Font(ESC_Params[f] - 10);
                break;
#if 0 // UNSUPPORTED               
            case 10: reset_Dim();
            case 11: set_Dim(); break;
#endif
            case 22: /* Bold off */
                Set_Attrib_Bold(0);
                break;
            case 24: /* Not underlined */
                Set_Attrib_Underline(0);
                break;
#if 0 // UNSUPPORTED         
            case 25: /* Not blinking */
                C_Atrrib.Blink = 0;
                break;
#endif
            case 27: /* Not reverse */
                Set_Attrib_Reverse(0);
                break;
#if defined(VT100_USE_COLOUR) && VT100_USE_COLOUR == 1
                Set_FColour(7); Set_BColour(0);
            case 30 ... 37:
                Set_FColour(ESC_Params[f] - 30);
                break;
            case 39: /* Default fg color */
                Set_FColour(7);
                break;
            case 40 ... 47:
                Set_BColour(ESC_Params[f] - 40);
                break;
            case 49: /* Default bg color */
                Set_BColour(0);
                break;
            case 90 ... 97:
                Set_FColour(ESC_Params[f] - 82);
                break;
            case 100 ... 107:
                Set_BColour(ESC_Params[f] - 92);
                break;
#endif
            default:
                break;
            }
        }
#endif
        break;
    case 'n':  // Reports
        switch (ESC_Params[0]) 
        {
            case 5: // REPORT Status
                // ESC [ 0 n   terminal OK
                // ESC [ 3 n   terminal not OK
#ifdef UART_ON
                uart_puts(UART_ID, "\e[0n");
#endif
                break;
            case 6: // REPORT cursor Position
                // ESC [ (Line Number) ; (Column Number) R 
                {
                char POS[11];
                sprintf(POS, "\e[%d;%dR", Cursor.Line + 1, Cursor.Column + 1 );
#ifdef UART_ON
                uart_puts(UART_ID, POS);
#endif
                }
                break;
        }
        break;
    case 'p':
        Set_Pallet(ESC_Params[0]);
        break;
    case 'q': // USER DEFINED LAMPS ESC [ 0 {RESET_ALL} ; 1 = {L1} ; 2 = {L2} ;  3 = {L3} ; 4 ={L4} q
        for (int f = 0; f <= Param_index; f++)
        switch (ESC_Params[f]) {
            case 0:
                for (int l = 0; l < 4; l++) Lamps[l] = 0;
                break;
            case 1: case 2: case 3: case 4: Lamps[ESC_Params[f] - 1] = 1; break;
            default: break;
        }
        // Redraw();
        break;
    case 'r':  // Set scrolling area ESC [ TOP; BOTTOM r 
    //RULES: at least 2 lines and TOP must less than BOTTOM
        if (( y = ESC_Params[0]) == 0) y = 1;
        if (( x = ESC_Params[1]) == 0) x = 1;
        Set_scrollregion(ESC_Params[0],ESC_Params[1]);
        Home(0); 
        break;
    default:
        break;
    }
    _ESC_RESET();
}
#endif 

#if 1 // Disable for now
void _ESC_CONF(char ch)
{
    bool state = ch == 'h' ? SET : RESET;
    switch (ESC_Params[0])
    {

    case 1: /* DECCKM = state; */ break;  // Cursor key mode     
    case 2: /* DECANM = state; */ break;  // ANSI / VT52 mode  **ON only
    case 3: break;  // 132/80 mode                                          NOT SUPPORTED
    case 4: break;  // Scrolling mode  Smooth/Jump                          NOT SUPPORTED
    case 5: break;  // Reverse/Normal Screen mode                           NOT SUPPORTED
    case 6: DECOM = state; break;  // Origin mode  Relative/Absolute
    case 7: DECAWM = state; break;  // Wrap around
    case 8: break;  // Auto repeat
    case 9: /* DECINLM = state; */ break;  // Keypad mode Application/Numeric  ** NUMLOCK???
    case 20: LNM = state; break; // Line feed/New Line
    case 25: Set_Cursor(state); break; // Show Hide cursor
    // These codes are non-standard
    case 50: break; // Local echo
    case 51: break; // Raw mode  **ON only
//    case 52: ST_Settings.scanlines = state; break;
//   case 53: ST_Settings._40Columns_mode = state; break;
//    case 54: ST_Settings.UI = state; break;
    default:
        break;
    }
    // Redraw();
}

void _ESC_CSI_Q(char ch)
{
    switch (ch)
    {
        case '0' ... '9':
            ESC_Params[Param_index] = 10 * ESC_Params[Param_index] + (ch - '0');
            return; 
        case 'h':
        case 'l':
            _ESC_CONF(ch);
        default:
            break;
    }
    _ESC_RESET();
}
#endif
#if 1 // Disable for now
void _ESC_G0(char ch) 
{ 
    switch (ch)
    {
        case 'A':  // UK SET
        case 'B':  // USASCII SET
        case '1':  // Alternate Character Set + Standard Set
            //Set_Attrib_Extended(false);
            break;
        case '0':  // Special Graphics
        case '2':  // Alternate Character Set + Special Graphics
            //Set_Attrib_Extended(true);
            break;
    }
    _ESC_RESET(); 
} 

void _ESC_G1(char ch) 
{ 
    switch (ch)
    {
        case 'A':  // UK SET
        case 'B':  // USASCII SET
        case '1':  // Alternate Character Set + Standard Set
            //Set_Attrib_Extended(false);
            break;  
        case '0':  // Special Graphics
        case '2':  // Alternate Character Set + Special Graphics
            //Set_Attrib_Extended(true);
            break;
    }
    _ESC_RESET();
}
#endif

#if defined(ENABLE_VT100) && ENABLE_VT100 >= 5
void _ESC_AMB(char ch)   // This is a royal pain to implement right now so leave it out for now.
{
    switch (ch)
    {
        case '3': // Top Half
            Line_Properties [cursorY] = 2;
            break;
        case '4': // Bottom Half
            Line_Properties [cursorY]  = 3;
            break;
        case '5': // Reset character size to normal for current row. 
            Line_Properties [cursorY] = 0;
            break;
        case '6': // Double wide single height
            Line_Properties [cursorY] = 1;
            break;
        default:
        break;
    }
    
    _ESC_RESET(); 
}
#endif

#if defined(ENABLE_VT100) && ENABLE_VT100 >= 1
//bool _Prase_ESC(char data)
bool _ESC_DECODE(char ch)
{
    if (ch <  0037 || ch == 0177)
    {
        return false;  // INVALID
    }
    switch (ESC_Mode)
    {
        case ESC_ONLY: _Prase_ESC(ch); break; // PT52 commands
        case ESC_CSI: _ESC_CSI(ch); break;
        case ESC_CSI_Q: _ESC_CSI_Q(ch); break;
        case ESC_G0: _ESC_G0(ch); break;
        case ESC_G1: _ESC_G1(ch); break;
#if ENABLE_VT100 >= 5  // Double height width
        case ESC_AMB: _ESC_AMB(ch); break;
#endif
        default: ESC_Mode = ESC_OFF;
    }
    return ESC_Mode != ESC_OFF ? true : false;
}
#endif