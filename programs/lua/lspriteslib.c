/*
    This is the PT52 sprites Library
*/

#define lspriteslib_c
#define LUA_LIB

#include "pt52io.h"
#include "pico/scanvideo.h"
extern const uint16_t Pallets[5][16]; // include"pallet.h" 

#include "lua.h"

#include "lauxlib.h"
#include "lualib.h"

#include <string.h>

#define LIBSPRITE_SPRITEMT "SPRITE"

static int SPRITE_set_pos(lua_State *L){
  uint16_t i = luaL_optinteger(L, 1, 0);
  uint16_t x = luaL_optinteger(L, 2, 0);
  uint8_t  y = luaL_optinteger(L, 3, 0);
  if (i > 16) return 0;
  _VBUFFERS.SPRITE_HEADERS[i].x = x;
  _VBUFFERS.SPRITE_HEADERS[i].y = y;
  return 0;
}
static int SPRITE_set_size(lua_State *L){
  uint16_t i = luaL_optinteger(L, 1, 0);
  uint8_t  w = luaL_optinteger(L, 2, 0);
  uint8_t  h = luaL_optinteger(L, 3, 0);
  if (i > 16) return 0;
  _VBUFFERS.SPRITE_HEADERS[i].h = h;
  _VBUFFERS.SPRITE_HEADERS[i].w = w;
  return 0;
}
static int SPRITE_set_datasize(lua_State *L){
  uint8_t i = luaL_optinteger(L, 1, 0);
  uint16_t  d = luaL_optinteger(L, 2, 0);
  if (i > 16) return 0;
  _VBUFFERS.SPRITE_HEADERS[i].data_length = d;
  return 0;
}
static int SPRITE_set_offset(lua_State *L){
  uint8_t i = luaL_optinteger(L, 1, 0);
  uint16_t  d = luaL_optinteger(L, 2, 0);
  if (i > 16) return 0;
  _VBUFFERS.SPRITE_HEADERS[i].data_offset = d;
  lua_pushinteger(L,_VBUFFERS.SPRITE_HEADERS[i].data_offset);
  return 1;
}
static int SPRITE_set_depth(lua_State *L){
  uint8_t i = luaL_optinteger(L, 1, 0);
  uint8_t  d = luaL_optinteger(L, 2, 0);
  if (i > 16) return 0;
  _VBUFFERS.SPRITE_HEADERS[i].colour_depth=d;
  return 0;
}
static int SPRITE_set_transparency(lua_State *L){
  uint8_t i = luaL_optinteger(L, 1, 0);
  uint8_t  d = luaL_optinteger(L, 2, 0);
  if (i > 16) return 0;
  _VBUFFERS.SPRITE_HEADERS[i].use_all_colours=d;
  return 0;
}
static int SPRITE_set_scaling(lua_State *L){
  uint8_t i = luaL_optinteger(L, 1, 0);
  uint8_t x = luaL_optinteger(L, 2, 0);
  uint8_t y = luaL_optinteger(L, 3, 0);
  if (i > 16) return 0;
  _VBUFFERS.SPRITE_HEADERS[i].scale_x=x;
  _VBUFFERS.SPRITE_HEADERS[i].scale_y=y;
  return 0;
}
static int SPRITE_get_pos(lua_State *L){
  uint16_t i = luaL_optinteger(L, 1, 0);
  if (i > 16) return 0;
  lua_pushinteger(L,_VBUFFERS.SPRITE_HEADERS[i].x);
  lua_pushinteger(L,_VBUFFERS.SPRITE_HEADERS[i].y);
  return 2;
}
static int SPRITE_move_pos(lua_State *L){
  uint16_t i = luaL_optinteger(L, 1, 0);
  int16_t x = luaL_optinteger(L, 2, 0);
  int16_t y = luaL_optinteger(L, 3, 0);
  if (i > 16) return 0;
  _VBUFFERS.SPRITE_HEADERS[i].x += x;
  _VBUFFERS.SPRITE_HEADERS[i].y += y;
  return 0;
}
static int SPRITE_shift_pallet(lua_State *L){
  uint8_t i = luaL_optinteger(L, 1, 0);
  uint8_t  d = luaL_optinteger(L, 2, 0);
  if (i > 16) return 0;
  _VBUFFERS.SPRITE_HEADERS[i].pallet_shift=d;
  return 0;
}
static int SPRITE_write_mem(lua_State *L){
  uint16_t i = luaL_optinteger(L, 1, 0);
  uint8_t  d = luaL_optinteger(L, 2, 0);
  if (i > 6144) return 0;
  *(_VBUFFERS.SPRITE_BUFF + i) = d;
  return 0;
}
static int SPRITE_read_mem(lua_State *L){
  uint16_t i = luaL_optinteger(L, 1, 0);
  if (i > 6144) {
    luaL_error(L,"OUT OF BOUNDS");
    return 0;
  }
  lua_pushinteger(L, *(_VBUFFERS.SPRITE_BUFF + i));
  return 1;
}
static int SPRITE_CLUT(lua_State *L){
  uint8_t  i = luaL_optinteger(L, 1, 0);
  uint8_t  r = luaL_optinteger(L, 2, 0);
  uint8_t  g = luaL_optinteger(L, 3, 0);
  uint8_t  b = luaL_optinteger(L, 4, 0);
  if (i > 255) {
    luaL_error(L,"OUT OF BOUNDS");
    return 0;
  }
  _VBUFFERS.SPRITE_CLUT[i] = PICO_SCANVIDEO_PIXEL_FROM_RGB8(r,g,b);
  return 0;
}
static int SPRITE_LOAD_CLUT(lua_State *L){
  uint8_t  i = luaL_optinteger(L, 1, 0);
  if (i > 4) {
    luaL_error(L,"OUT OF BOUNDS");
  return 0;
  }
  memcpy(_VBUFFERS.SPRITE_CLUT,Pallets[i],sizeof(uint16_t) * 16);
  return 0;
}
static int SPRITE_get(lua_State *L){
  uint8_t  i = luaL_optinteger(L, 1, 0);
  if (i > 16) {
    lua_pushnil(L);
  } else {
    lua_pushlightuserdata(L,&_VBUFFERS.SPRITE_HEADERS[i]);
    luaL_setmetatable(L, LIBSPRITE_SPRITEMT);
  }
  return 1;
}
static int sprite_t_str(lua_State *L){
  lua_pushstring(L, "SPRITE");
  return 1;
}
static int sprite_t_pos(lua_State *L) {
  if (!lua_isuserdata(L,1)) luaL_error(L,"Sprite missing!");
  _SPRITE_HEADER_T *d = (_SPRITE_HEADER_T *)lua_touserdata(L, 1);
  if (!lua_isnoneornil(L,2))
  {
    d->x = luaL_optinteger(L,2,0);
  }
  if (!lua_isnoneornil(L,3))
  {
    d->y = luaL_optinteger(L,3,0);
  }
  lua_pushinteger(L,d->x);
  lua_pushinteger(L,d->y);
  return 2;
}
static int sprite_t_size(lua_State *L) {
  if (!lua_isuserdata(L,1)) luaL_error(L,"Sprite missing!");
  _SPRITE_HEADER_T *d = (_SPRITE_HEADER_T *)lua_touserdata(L, 1);
  if (!lua_isnoneornil(L,2))
  {
    d->w = luaL_optinteger(L,2,0);
  }
  if (!lua_isnoneornil(L,3))
  {
    d->h = luaL_optinteger(L,3,0);
  }
  lua_pushinteger(L,d->w);
  lua_pushinteger(L,d->h);
  return 2;
}
static int sprite_t_len(lua_State *L) {
  if (!lua_isuserdata(L,1)) luaL_error(L,"Sprite missing!");
  _SPRITE_HEADER_T *d = (_SPRITE_HEADER_T *)lua_touserdata(L, 1);
  if (!lua_isnoneornil(L,2))
  {
    d->data_length = luaL_optinteger(L,2,0);
  }
  lua_pushinteger(L,d->data_length);
  return 1;
}
static int sprite_t_depth(lua_State *L) {
  if (!lua_isuserdata(L,1)) luaL_error(L,"Sprite missing!");
  _SPRITE_HEADER_T *d = (_SPRITE_HEADER_T *)lua_touserdata(L, 1);
  if (!lua_isnoneornil(L,2))
  {
    d->colour_depth = luaL_optinteger(L,2,0);
  }
  lua_pushinteger(L,d->colour_depth);
  return 1;
}
static int sprite_t_transparent(lua_State *L) {
  if (!lua_isuserdata(L,1)) luaL_error(L,"Sprite missing!");
  _SPRITE_HEADER_T *d = (_SPRITE_HEADER_T *)lua_touserdata(L, 1);
  if (!lua_isnoneornil(L,2))
  {
    d->use_all_colours = ~(luaL_optinteger(L,2,0) & 1);
  }
  lua_pushinteger(L,d->use_all_colours);
  return 1;
}
static int sprite_t_move(lua_State *L) {
  if (!lua_isuserdata(L,1)) luaL_error(L,"Sprite missing!");
  _SPRITE_HEADER_T *d = (_SPRITE_HEADER_T *)lua_touserdata(L, 1);
  int16_t x = luaL_optinteger(L, 2, 0);
  int16_t y = luaL_optinteger(L, 3, 0);
  d->x += x;
  d->y += y;
  return 0;
}
static int sprite_t_scale(lua_State *L) {
  if (!lua_isuserdata(L,1)) luaL_error(L,"Sprite missing!");
  _SPRITE_HEADER_T *d = (_SPRITE_HEADER_T *)lua_touserdata(L, 1);
  if (!lua_isnoneornil(L,2))
  {
    d->scale_x = luaL_optinteger(L,2,0);
  }
  if (!lua_isnoneornil(L,3))
  {
    d->scale_y = luaL_optinteger(L,3,0);
  }
  lua_pushinteger(L,d->scale_x);
  lua_pushinteger(L,d->scale_y);
  return 2;
}
static int sprite_t_offset(lua_State *L) {
  if (!lua_isuserdata(L,1)) luaL_error(L,"Sprite missing!");
  _SPRITE_HEADER_T *d = (_SPRITE_HEADER_T *)lua_touserdata(L, 1);
  if (!lua_isnoneornil(L,2))
  {
    d->data_offset = luaL_optinteger(L,2,0);
  }
  lua_pushinteger(L,d->data_offset);
  return 1;
}
static int sprite_t_palletshift(lua_State *L){
  if (!lua_isuserdata(L,1)) luaL_error(L,"Sprite missing!");
  _SPRITE_HEADER_T *d = (_SPRITE_HEADER_T *)lua_touserdata(L, 1);
  if (!lua_isnoneornil(L,2))
  {
    d->pallet_shift = luaL_optinteger(L,2,0);
  }
  lua_pushinteger(L,d->pallet_shift);
  return 1;
}
static const luaL_Reg meth[] = {
  {"pos",         sprite_t_pos},
  {"size",        sprite_t_size},
  {"len",         sprite_t_len},
  {"depth",       sprite_t_depth},
  {"move",        sprite_t_move},
  {"transparent", sprite_t_transparent},
  {"scale",       sprite_t_scale},
  {"offset",      sprite_t_offset},
  {"palletshift", sprite_t_palletshift},
  {NULL, NULL}
};
static const luaL_Reg metameth[] = {
  {"__index",     NULL},
  {"__tostring",  sprite_t_str},
  {NULL, NULL}
};

static const luaL_Reg spriteslib[] = {
  {"get",           SPRITE_get},
  {"pos",           SPRITE_set_pos},
  {"size",          SPRITE_set_size},
  {"length",        SPRITE_set_datasize},
  {"offset",        SPRITE_set_offset},
  {"depth",         SPRITE_set_depth},
  {"transparent",   SPRITE_set_transparency},
  {"move",          SPRITE_move_pos},
  {"set_scaling",   SPRITE_set_scaling},
  {"shift_pallet",  SPRITE_shift_pallet},
  {"get_pos",       SPRITE_get_pos},
  {"vpoke",         SPRITE_write_mem},
  {"vpeek",         SPRITE_read_mem},
  {"set_colour",    SPRITE_CLUT},
  {"pallet",        SPRITE_LOAD_CLUT},
  {NULL, NULL}
};

/* }====================================================== */
static void create_sprite_meta_table (lua_State *L) {
  luaL_newmetatable(L, LIBSPRITE_SPRITEMT);
  luaL_setfuncs(L, metameth, 0);  /* add metamethods to new metatable */
  luaL_newlibtable(L, meth);  /* create method table */
  luaL_setfuncs(L, meth, 0);  /* add file methods to method table */
  lua_setfield(L, -2, "__index");  /* metatable.__index = method table */
  lua_pop(L, 1);  /* pop metatable */
}

LUAMOD_API int luaopen_sprites (lua_State *L) {
    luaL_newlib(L, spriteslib);
    create_sprite_meta_table(L);
    return 1;
}