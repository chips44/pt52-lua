// Lua PT52 configurations 
#ifndef _LPT52_CONF_H_
#define _LPT52_CONF_H_

#define LUA_PROMPT		"] "
#define LUA_PROMPT2		"+] "

/* Lua Print functions */

#define lua_writestring(s,l)   Printf(("%s",s))

/* print a newline */
#define lua_writeline()        (Puts(""))

/* print an error message */
#define lua_writestringerror(s,l) (Printf((s),(l)),Puts(""))

int Printf(const char *format, ...);
void Puts(const char *s);
#endif