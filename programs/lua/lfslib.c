
#define lfslib_c
#define LUA_LIB

#include "pt52io.h"

#include "lua.h"

#include "lauxlib.h"
#include "lualib.h"

#define L_CONST(_f)	(void)( {			\
					lua_pushinteger(L, _f);		\
					lua_setfield(L, -2, #_f);	\
				} )
#if 0

/* forward declaration for the iterator function */
static int dir_iter (lua_State *L);

static int l_dir (lua_State *L) {
  const char *path = luaL_checkstring(L, 1);

  /* create a userdatum to store a DIR address */
  DIR **d = (DIR **)lua_newuserdata(L, sizeof(DIR *));

  /* set its metatable */
  luaL_getmetatable(L, "LuaBook.dir");
  lua_setmetatable(L, -2);

  /* try to open the given directory */
  *d = opendir(path);
  if (*d == NULL)  /* error opening the directory? */
	luaL_error(L, "cannot open %s: %s", path,
										strerror(errno));

  /* creates and returns the iterator function
	 (its sole upvalue, the directory userdatum,
	 is already on the stack top */
  lua_pushcclosure(L, dir_iter, 1);
  return 1;
}


static int dir_iter (lua_State *L) {
  DIR *d = *(DIR **)lua_touserdata(L, lua_upvalueindex(1));
  struct dirent *entry;
  if ((entry = readdir(d)) != NULL) {
	lua_pushstring(L, entry->d_name);
	return 1;
  }
  else return 0;  /* no more values to return */
}

static int dir_gc (lua_State *L) {
  DIR *d = *(DIR **)lua_touserdata(L, 1);
  if (d) closedir(d);
  return 0;
}

int luaopen_dir (lua_State *L) {
  luaL_newmetatable(L, "LuaBook.dir");

  /* set its __gc field */
  lua_pushstring(L, "__gc");
  lua_pushcfunction(L, dir_gc);
  lua_settable(L, -3);

  /* register the `dir' function */
  lua_pushcfunction(L, l_dir);
  lua_setglobal(L, "dir");

  return 0;
}
#endif
typedef struct {
  DIR ptr;
  uint8_t is_closed:1;
  uint8_t is_top:1;
} LIBFS_DIR_T;

#define LIBFS_DIR_T_size sizeof(LIBFS_DIR_T)
#define LIBFS_DIRMT "DIR*"

static int fs_dir(lua_State* L) {
    const char* path = luaL_checkstring(L, 1);
    const char* pattern = luaL_optstring(L, 2, "*");
    // Create a userdatum to store a DIR address
    LIBFS_DIR_T* d = (LIBFS_DIR_T*)lua_newuserdata(L, sizeof(LIBFS_DIR_T));
    d->is_top = true;
    // Set its metatable
    luaL_setmetatable(L, LIBFS_DIRMT);

    FILINFO fno; /* File information */
    if (f_findfirst(&d->ptr, &fno, path, pattern) != FR_OK) {
        // Error opening the directory
        luaL_error(L, "cannot open %s", path);
        f_closedir(&d->ptr);
        return 0;
    }

    // Return the userdatum
    return 1;
}
static int fs_file_tostr(lua_State *L) {
    lua_pushstring(L, "name");
    lua_gettable(L, 1);
    return 1;
}

// Directory iterator function
static int fs_dir_iter (lua_State *L) {
  if (!lua_isuserdata(L,1)) luaL_error(L,"DIR* MISSING!");
  LIBFS_DIR_T *d = (LIBFS_DIR_T *)lua_touserdata(L, 1);
  if (d->is_top) { f_readdir(&d->ptr, NULL); d->is_top = false; }
  FILINFO fno; /* File information */
  if (f_findnext(&d->ptr, &fno) == FR_OK && fno.fname[0]) {
    lua_newtable(L);  // Create the main table
    
    lua_pushstring(L, "name");
    lua_pushstring(L, fno.fname);
    lua_settable(L, -3);
    
    lua_pushstring(L, "size");
    lua_pushinteger(L, fno.fsize);
    lua_settable(L, -3);
    
    lua_pushstring(L, "attrib");
    lua_pushinteger(L, fno.fattrib);
    lua_settable(L, -3);

    lua_newtable(L);
    lua_pushstring(L, "__tostring");
    lua_pushcfunction(L, fs_file_tostr);
    lua_settable(L, -3);
    
    lua_setmetatable(L, -2);

	  return 1;
  }
  return 0;  /* no more values to return */
}
// Directory Garbage Collet
static int fs_dir_rew (lua_State *L) {
  LIBFS_DIR_T *d = (LIBFS_DIR_T *)lua_touserdata(L, 1);
  if (!d->is_closed) 
  {
     f_readdir(&d->ptr, NULL);
     lua_pushboolean(L, true);
     return 1;
  }
  luaL_error(L, "Directory is closed");
  return 0;
}
static int fs_dir_gc (lua_State *L) {
  LIBFS_DIR_T *d = (LIBFS_DIR_T *)lua_touserdata(L, 1);
  if (!d->is_closed) 
  {
    f_closedir(&d->ptr);
    d->is_closed = true;
  }
  return 0;
}
// Directory to String
static int fs_dir_str (lua_State *L) {
  LIBFS_DIR_T *d = (LIBFS_DIR_T *)lua_touserdata(L, 1);
  if (!d->is_closed) lua_pushfstring(L, "DIR* (%p)", d);
  else lua_pushstring(L, "DIR* (CLOSED)");
  return 1;
}
 
static int fs_stat(lua_State* L) {
  FILINFO fno;
  FRESULT fr = f_stat(luaL_checkstring(L, 1), &fno);
  if (FR_OK != fr) 
  {
      luaL_error(L, "Invalid directory");
      return 1;
  }
  lua_newtable(L);  // Create the main table
    
  lua_pushstring(L, "name");
  lua_pushstring(L, fno.fname);
  lua_settable(L, -3);
  
  lua_pushstring(L, "size");
  lua_pushinteger(L, fno.fsize);
  lua_settable(L, -3);
  
  lua_pushstring(L, "attrib");
  lua_pushinteger(L, fno.fattrib);
  lua_settable(L, -3);

  lua_newtable(L);
  lua_pushstring(L, "__tostring");
  lua_pushcfunction(L, fs_file_tostr);
  lua_settable(L, -3);
  
  lua_setmetatable(L, -2);
  return 1;
}

static int fs_chdir(lua_State* L) {
  FRESULT fr = f_chdir(luaL_checkstring(L, 1));
  if (FR_OK != fr) 
  {
      luaL_error(L, "Invalid directory");
      return 1;
  }
  lua_pushboolean(L,1);
  return 1;
}

static int fs_pwd(lua_State* L) {
  FRESULT fr;
  char str[80];
  fr = f_getcwd(str, 80);
  if (FR_OK != fr) lua_pushnil(L);
  else lua_pushstring(L,str);
  return 1;
}

static int fs_mkdir(lua_State* L) {
  FRESULT fr = f_mkdir(luaL_checkstring(L, 1));
  if (FR_OK != fr) {
      luaL_error(L, "Couldn't create dir");
      return 1;
  }
  lua_pushboolean(L,1);
  return 1;
}

static int fs_rmdir(lua_State* L) {
  FRESULT fr = f_rmdir(luaL_checkstring(L, 1));
  if (FR_OK != fr) {
      luaL_error(L, "Couldn't remove dir");
      return 1;
  }
  lua_pushboolean(L,1);
  return 1;
}
static int fs_copy(lua_State* L) {
  return 0;
}

static const luaL_Reg meth[] = {
  {"next",    fs_dir_iter},
  {"close",   fs_dir_gc},
  {"rewind",  fs_dir_rew},
  {NULL, NULL}
};

static const luaL_Reg metameth[] = {
  {"__index", NULL},  /* place holder */
  {"__gc", fs_dir_gc},
  {"__close", fs_dir_gc},
  {"__tostring", fs_dir_str},
  {"__call", fs_dir_iter},
  {NULL, NULL}
};

static const luaL_Reg fslib[] = {
  {"chdir",   fs_chdir},
  {"mkdir",   fs_mkdir},
  {"rmdir",   fs_rmdir},
//  {"move",  fs_move},   // exists in the OS library
//  {"copy",  fs_copy},   // rethink??
  {"stat",    fs_stat},
  {"pwd",     fs_pwd},
  {"dir",     fs_dir},
  {NULL, NULL}
};

static void create_dir_meta_table (lua_State *L) {
  luaL_newmetatable(L, LIBFS_DIRMT);
  luaL_setfuncs(L, metameth, 0);  /* add metamethods to new metatable */
  luaL_newlibtable(L, meth);  /* create method table */
  luaL_setfuncs(L, meth, 0);  /* add file methods to method table */
  lua_setfield(L, -2, "__index");  /* metatable.__index = method table */

  lua_pop(L, 1);  /* pop metatable */
}
int luaopen_fs(lua_State* L) {

  luaL_newlib(L, fslib);
  create_dir_meta_table(L);
  
  L_CONST(AM_RDO);
  L_CONST(AM_HID);
  L_CONST(AM_SYS);
  L_CONST(AM_DIR);
  L_CONST(AM_ARC);
  return 1;
}
/**
local fatfs = require("fatfs")

for file in fatfs.directory("./path/to/directory") do
    print(file.size, file.attributes)
end

*/