/*
    This is the PT52 overlay Library
*/

#define ldrawlib_c
#define LUA_LIB

#include "pt52io.h"
#include "pico/scanvideo.h"
extern const uint16_t Pallets[5][16]; // include"pallet.h" 

#include "lua.h"

#include "lauxlib.h"
#include "lualib.h"

#include <string.h>
static int OVERLAY_clear_screen(lua_State *L) {
  uint8_t c = luaL_optinteger(L, 1, 0) & 0xF;
  Overlay_clear(c);
  return 0;
}
static int OVERLAY_line(lua_State *L) {
  uint16_t x0 = luaL_optinteger(L, 1, 0);
  uint8_t  y0 = luaL_optinteger(L, 2, 0);
  uint16_t x1 = luaL_optinteger(L, 3, 0);
  uint8_t  y1 = luaL_optinteger(L, 4, 0);
  uint8_t  c = luaL_optinteger(L, 5, 0);
  Overlay_draw_line(x0, y0, x1, y1, c);
  return 0;
}
static int OVERLAY_rect(lua_State *L) {
  uint16_t x = luaL_optinteger(L, 1, 0);
  uint8_t  y = luaL_optinteger(L, 2, 0);
  uint16_t w = luaL_optinteger(L, 3, 0);
  uint8_t  h = luaL_optinteger(L, 4, 0);
  uint8_t  c = luaL_optinteger(L, 5, 0);
  uint8_t  f = luaL_optinteger(L, 6, -1);
  if (f >= 0) Overlay_fill_rect(x, y, w, h, f);
  Overlay_draw_rect(x,  y,  w,  h,  c);
  return 0;
}
static int OVERLAY_roundrect(lua_State *L) {
  uint16_t x = luaL_optinteger(L, 1, 0);
  uint8_t  y = luaL_optinteger(L, 2, 0);
  uint16_t w = luaL_optinteger(L, 3, 0);
  uint8_t  h = luaL_optinteger(L, 4, 0);
  uint8_t  r = luaL_optinteger(L, 5, 0); 
  uint8_t  c = luaL_optinteger(L, 6, 0); 
  Overlay_drawRoundRect(x,  y,  w,  h,  r,  c);
  return 0;
}
static int OVERLAY_circle(lua_State *L) {
  uint16_t x = luaL_optinteger(L, 1, 0);
  uint8_t  y = luaL_optinteger(L, 2, 0);
  uint16_t r = luaL_optinteger(L, 3, 0);
  uint8_t  c = luaL_optinteger(L, 4, 0);
  uint8_t  f = luaL_optinteger(L, 5, -1);
  if (f >= 0) Overlay_fill_circle(x, y, r, f);
  Overlay_draw_circle(x,  y,  r,  c);
  return 0;
}
static int OVERLAY_ellipse(lua_State *L) {
  uint16_t xc = luaL_optinteger(L, 1, 0);
  uint8_t  yc = luaL_optinteger(L, 2, 0);
  uint16_t rx = luaL_optinteger(L, 3, 0);
  uint16_t ry = luaL_optinteger(L, 4, 0);
  uint8_t  c  = luaL_optinteger(L, 5, 0);
  Overlay_draw_ellipse(xc,  yc,  rx,  ry,  c);
  return 0;
}
static int OVERLAY_pixel(lua_State *L) {
  uint16_t x = luaL_optinteger(L, 1, 0);
  uint8_t  y = luaL_optinteger(L, 2, 0);
  uint8_t  c = luaL_optinteger(L, 3, 0);
  Overlay_pixel_plotter(x, y, c);
  return 0;
}
static int OVERLAY_char(lua_State *L) {
  uint8_t  x = luaL_optinteger(L, 1, 0);
  uint8_t  y = luaL_optinteger(L, 2, 0);
  uint8_t  c = luaL_optinteger(L, 3, 0);
  uint8_t  F = luaL_optinteger(L, 4, 0);
  uint8_t ch = luaL_optinteger(L, 5, 0);
  Overlay_putchar(x, y, c, F, ch);
  return 0;
}
static int OVERLAY_string(lua_State *L) {
  uint8_t       x = luaL_optinteger(L, 1, 0);
  uint8_t       y = luaL_optinteger(L, 2, 0);
  uint8_t       c = luaL_optinteger(L, 3, 0);
  const char* str = luaL_optstring(L, 4, "");
  Overlay_print(x, y, c, "%s", str);
  return 0;
}
static int OVERLAY_CLUT(lua_State *L) {
  uint8_t  i = luaL_optinteger(L, 1, 0);
  uint8_t  r = luaL_optinteger(L, 2, 0);
  uint8_t  g = luaL_optinteger(L, 3, 0);
  uint8_t  b = luaL_optinteger(L, 4, 0);
  if (i > 15) {
    luaL_error(L,"OUT OF BOUNDS");
    return 0;
  }
  _VBUFFERS.OVRLY_CLUT[i] = PICO_SCANVIDEO_PIXEL_FROM_RGB8(r,g,b);
  return 0;
}
static int OVERLAY_LOAD_CLUT(lua_State *L) {
  uint8_t  i = luaL_optinteger(L, 1, 0); 
  if (i < 2) {
    Overlay_load_clut(1);
    return 0;
  }
  i -= 2;
  if (i > 4) {
      luaL_error(L,"OUT OF BOUNDS");
      return 0;
    }
    memcpy(_VBUFFERS.OVRLY_CLUT,Pallets[i],sizeof(uint16_t) * 16);
  return 0;
}
static int OVERLAY_init(lua_State *L) {
  Overlay_init(0);
  return 0;
}
struct OVERLAY_FLAGS_T {
  uint8_t   index;
  uint8_t   mask;
  uint8_t   shift;
};
static const char *const OVERLAY_REGISTERS[] = {
  "x", "y", "w", "h", "clut", "scale x", "scale y",
  "enable", "transparent", "protected", "window",
  NULL
};
static const struct OVERLAY_FLAGS_T OVERLAY_REGISTERS_MAP[] = {
  {0, 0xff, 0},
  {1, 0xff, 0},
  {2, 0xff, 0},
  {3, 0xff, 0},
  {4, 0x03, 0},
  {4, 0x04, 2},
  {4, 0x08, 3},
  {4, 0x10, 4},
  {4, 0x20, 5},
  {4, 0x40, 6},
  {5, 0xff, 0},
};

static int OVERLAY_reg(lua_State *L) {
  int index = luaL_checkoption(L,1,OVERLAY_REGISTERS[0],OVERLAY_REGISTERS);
  if (lua_isnoneornil(L,2)) {
    uint8_t value = _VBUFFERS.BUFFER_RAW[0x3E80 + 276 + OVERLAY_REGISTERS_MAP[index].index];
    value &=  OVERLAY_REGISTERS_MAP[index].mask;
    value >>= OVERLAY_REGISTERS_MAP[index].shift;
    lua_pushinteger(L, value);
    return 1;
  }
  uint8_t value = (luaL_optinteger(L,2,0) << OVERLAY_REGISTERS_MAP[index].shift) & OVERLAY_REGISTERS_MAP[index].mask;
  _VBUFFERS.BUFFER_RAW[0x3E80 + 276 + OVERLAY_REGISTERS_MAP[index].index] &= ~OVERLAY_REGISTERS_MAP[index].mask;
  _VBUFFERS.BUFFER_RAW[0x3E80 + 276 + OVERLAY_REGISTERS_MAP[index].index] |= value;
  return 0;
}

static const luaL_Reg overlaylib[] = {
  {"reset",     OVERLAY_init},
  {"clr",       OVERLAY_clear_screen},
  {"line",      OVERLAY_line},
  {"rect",      OVERLAY_rect},
  {"roundrect", OVERLAY_roundrect},
  {"circle",    OVERLAY_circle},
  {"ellipse",   OVERLAY_ellipse},
  {"pixel",     OVERLAY_pixel},
  {"putchar",   OVERLAY_char},
  {"print",     OVERLAY_string},
  {"set_colour",OVERLAY_CLUT},
  {"pallet",    OVERLAY_LOAD_CLUT},
  {"reg",       OVERLAY_reg},
  {NULL, NULL}
};

/* }====================================================== */



LUAMOD_API int luaopen_overlay (lua_State *L) {
    luaL_newlib(L, overlaylib);
    return 1;
}